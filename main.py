def greet(name: str):
    print("Hello", name)


def main():
    greet("World")
    greet("Vasya")


if __name__ == '__main__':
    main()
